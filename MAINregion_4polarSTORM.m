clear all
close all
clc

% Main entry point
setappdata(0,'usediplib',false)
setappdata(0,'staticplugins',isdeployed)
setappdata(0,'initialized',isdeployed)

% 
 [filename, pathname] = uigetfile( ...
       {'*.mat'}, ...
        'Pick a file');
    
originalname=[pathname filename];
data=load([pathname filename]);
data.originalname=originalname;

factor0_45=data.factor0_45;
factorg1=data.factorg1;
factorg2=data.factorg2;
factorg45=data.factorg45;
factorg90=data.factorg90;

X=data.X;
Y=data.Y;
T = data.T;
Intensity=data.Intensity;
Z = 10.*(Intensity/max(Intensity(:)));

Dummy_sigma_X=1.*ones(size(X));
Dummy_sigma_Y=1.*ones(size(X));
rho_all=(data.rho_all);
delta_all=(data.delta_all);
numAreas=0;

%% Extra data
sigma_0=data.sigma_0;
sigma_45=data.sigma_45;
sigma_90=data.sigma_90;
sigma_135=data.sigma_135;

sigmabg=data.sigmabg;
sigmabg_0=data.sigmabg_0;
sigmabg_45=data.sigmabg_45;
sigmabg_90=data.sigmabg_90;
sigmabg_135=data.sigmabg_135;

P0vect_all=data.P0vect_all;
P45vect_all=data.P45vect_all;

I0=data.I0;
I45=data.I45;
I90=data.I90;
I135=data.I135;

radius_0=data.radius_0;
radius_45=data.radius_45;
radius_90=data.radius_90;
radius_135=data.radius_135;

subset4x4=data.subset4x4;

%% Removing nan
if or(sum(isnan(rho_all))>0,sum(isnan(delta_all))>0)
    selnonan=(~(isnan(rho_all))==1)&(~(isnan(delta_all))==1);
    X=X(selnonan);
    Y=Y(selnonan);
    T=T(selnonan);
    Intensity=Intensity(selnonan);
    Z =Z(selnonan);
    Dummy_sigma_X=Dummy_sigma_X(selnonan);
    Dummy_sigma_Y=Dummy_sigma_Y(selnonan);
    rho_all=rho_all(selnonan);
    delta_all=delta_all(selnonan);
    
    sigma_0=sigma_0(selnonan);
    sigma_45=sigma_45(selnonan);
    sigma_90=sigma_90(selnonan);
    sigma_135=sigma_135(selnonan);
    
    sigmabg_0=sigmabg_0(selnonan);
    sigmabg_45=sigmabg_45(selnonan);
    sigmabg_90=sigmabg_90(selnonan);
    sigmabg_135=sigmabg_135(selnonan);
    sigmabg=sigmabg(selnonan);

    P0vect_all=P0vect_all(selnonan);
    P45vect_all=P45vect_all(selnonan);

    I0=I0(selnonan);
    I45=I45(selnonan);
    I90=I90(selnonan);
    I135=I135(selnonan);

    radius_0=radius_0(selnonan);
    radius_45=radius_45(selnonan);
    radius_90=radius_90(selnonan);
    radius_135=radius_135(selnonan);
    subset4x4=subset4x4(selnonan);
end 
%%



clearvars data

PALMsiever('X','Y')


