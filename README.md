# 4polar-STORM

`4polar-STORM` is a Matlab software for the analysis and visualization of molecular orientation in STORM polarized super-resolution imaging of actin filaments organization in cells. 

- [Overview](#overview)
- [System Requirements](#system-requirements)
- [Installation Guide](#installation-guide)
- [Instructions](#instructions)
- [Demo dataset](#demo-dataset)
- [References](#references)
- [License](#license)
- [Contact](#contact)

# Overview

4polar-STORM Graphical User Interface includes the estimation of molecular orientation (ρ and δ), as well as their graphical representation as sticks. In this representation, ρ is depicted as the orientation angle of sticks (with respect to the horizontal axis of the image), and δ or ρ are depicted as the colors of the sticks. The sticks are displayed over a black-and-white image representing the super-resolved STORM image, which uses the localization of all detected molecules.

This program is developed based on PALMsiever, DIPImage, polar dSTORM and MTT tracking (detection module).

The Graphical Interface and more functions are under development.

# System Requirements

## Hardware Requirements

4polar-STORM requires a workstation computer with multiprocessor CPU and enough RAM (>64 Go). 

For optimal performance, we recommend the following specifications:

RAM: >64 GB

CPU: >6 cores, >2.7 GHz/core

Runtimes below are generated using a computer with 64 Go and 6 cores @2.7 Ghz (Intel(R) Xeon(R) E-2176M  CPU).

## Software Requirements

4polarSTORM was developed on Windows 10 with Matlab R2020b(Mathworks(R)). The performance was tested on Windows 7/10. It should work with MAC and Linux systems with appropriate Matlab installation.

These Matlab toolboxes are necessary for the software: Curve Fitting, Signal Processing and Image Processing.

DipImage has to be installed. It can be downloaded from https://diplib.org/

# Installation Guide

1. Make sure that DipImage is installed and take a note of the installation directory(diplib_directory).
2. Download the source zip file from https://gitlab.fresnel.fr/mosaic/4polarSTORM/
3. Unzip the source file in a directory called “4polarSTORM”.
4. Open Matlab.
5. Change Matlab working directory to 4polarSTORM directory.
6. Modify the variable “diplib_directory” in the palmsiever_setup.m file with your DipImage installation directory.
7. Execute the PRESSFIRST.m script. This allows to set the necessary paths for the software. 

The installation is simple and it does not require long time. 

# Instructions

1. Execute the PRESSFIRST.m script. 

2. Start the 4polar-STORM software by running the script MAIN_4polarSTORM.m 

3. This script will request the output matfile from the detection algorithm. A sample test data is available in the folder "TestData".
    For demo, select the file: np_ctrl_CA_CLMK_cell101_pFAK_4polarSTORM_24_r_1.3_W_13_in_4x4_Cropped6500.mat 


It is also possible to run the software in a previous selected region using MAIN_4polarSTORM.m:

1. Execute the PRESSFIRST.m script. 

2. Start the 4polar-STORM software for cropped regions by running the script MAINregion_4polarSTORM.m

3. This script will request the matfile of a region previously saved in MAIN_4polarSTORM. Two regions are available in the folder "TestData".

    Region A: np_ctrl_CA_CLMK_cell101_pFAK_4polarSTORM_24_r_1.3_W_13_in_4x4_Cropped6500_Region_1.mat

    Region B: np_ctrl_CA_CLMK_cell101_pFAK_4polarSTORM_24_r_1.3_W_13_in_4x4_Cropped6500_Region_2.mat


# Demo dataset

A demo dataset is included in the folder "TestData". This file includes the quantitative parameters (position, localization precision, amplitude, radius, and noise) of the blinking particles per polarized projection. Particles were already registered and associated. Demo dataset includes only particles from the first 6500 timepoints. Processing time of this dataset is (~25 seconds).

**Sample:** U2OS CA-MLCK (AF488-phalloidin-labelled F-actin) . **Initial fitting radius**: 1.3 pixels. **Detection window**: 13 pixels.**Probability of false alarm**: 24. **Pixel size**: 130 nm. **Exposure time**: 100 ms. **Camera gain**: 300. **Camera**: Andor Ixon 888 EMCCD

Full dataset:
np_ctrl_CA_CLMK_cell101_pFAK_4polarSTORM_24_r_1.3_W_13_in_4x4_Cropped6500.mat  

Region A:
np_ctrl_CA_CLMK_cell101_pFAK_4polarSTORM_24_r_1.3_W_13_in_4x4_Cropped6500_Region_1.mat

Region B:
np_ctrl_CA_CLMK_cell101_pFAK_4polarSTORM_24_r_1.3_W_13_in_4x4_Cropped6500_Region_2.mat

# References

- 4-polarSTORM has been published as preprint in https://www.biorxiv.org/content/10.1101/2021.03.17.435879v1

- PALMsiever is available at https://github.com/PALMsiever/palm-siever

    Pengo, T., Holden, S. J. & Manley, S. PALMsiever: A tool to turn raw data into results for single-molecule localization microscopy. Bioinformatics 31, 797–798 (2015).

- DIPImage is available at https://diplib.org/

- polar-dSTORM workflow is described in https://www.pnas.org/content/113/7/E820

    Valades Cruz, C. A. et al. Quantitative nanoscale imaging of orientational order in biological filaments by polarized superresolution microscopy. Proc. Natl. Acad. Sci. U.S.A. 113, (2016).

- MTT software has been published in https://www.nature.com/articles/nmeth.1233
    
    Sergé, A., Bertaux, N., Rigneault, H. & Marguet, D. Dynamic multiple-target tracing to probe spatiotemporal cartography of cell membranes. Nat. Methods (2008).

# License

This project is covered under the **GNU General Public License v3.0.**

# Contact

For any questions, please contact: sophie.brasselet[AT]fresnel.fr and cesar.valades[AT]gmail.com
