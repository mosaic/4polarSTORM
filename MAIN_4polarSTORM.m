clear all
close all
clc

warning off;

% Main entry point
setappdata(0,'usediplib',false)
setappdata(0,'staticplugins',isdeployed)
setappdata(0,'initialized',isdeployed)


% 
 [filename, pathname] = uigetfile( ...
       pwd, ...
        'Pick a file');
    
originalname=[pathname filename];


data=load([pathname filename],'tab_traj_4x4','param_t_j','param_t_i','param_t_r0','param_t_alpha','param_t_sig2','param_t_sig2_b','tab_trajcoupled','tab_traj_allcoupled','param_t_tps');
data.originalname=originalname;
j1=floor(min(data.tab_traj_4x4(data.param_t_j, 1:4:end)));
j2=ceil(max(data.tab_traj_4x4(data.param_t_j, 1:4:end)));
i1=floor(min(data.tab_traj_4x4(data.param_t_i, 1:4:end)));
i2=ceil(max(data.tab_traj_4x4(data.param_t_i, 1:4:end)));

prompt = {'G1: ','G2: ', 'G45: ', 'G90: ','G0_45'};
dlg_title = 'Input';
num_lines = 1;
defaultans = {'0.1879','0.2120','1.0396','0.9944','1.3413'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

factorg1=str2double(answer{1});
factorg2=str2double(answer{2});
factorg45=str2double(answer{3});
factorg90=str2double(answer{4});
factor0_45=str2double(answer{5});

[delta_all,rho_all,posx,posy,It,sigma_0,sigma_45,sigma_90,sigma_135,P0vect_all,P45vect_all,I0,I45,I90,I135,frame,radius_0,radius_45,radius_90,radius_135,sigmabg_0,sigmabg_90,sigmabg_45,sigmabg_135,sigmabg] = getvalues4x4(data.tab_traj_4x4,factorg1,factorg2, factorg45, factorg90, factor0_45);

X=posx';
Y=posy';
T = frame';


Intensity=It';
Z = 10.*(Intensity/max(Intensity(:)));

Dummy_sigma_X=1.*ones(size(X));
Dummy_sigma_Y=1.*ones(size(X));
rho_all=(rho_all*180/pi)';
delta_all=(delta_all*180/pi)';
numAreas=0;

%% Extra data
sigma_0=sigma_0';
sigma_45=sigma_45';
sigma_90=sigma_90';
sigma_135=sigma_135';

sigmabg=sigmabg';
sigmabg_0=sigmabg_0';
sigmabg_45=sigmabg_45';
sigmabg_90=sigmabg_90';
sigmabg_135=sigmabg_135';


P0vect_all=P0vect_all';
P45vect_all=P45vect_all';

I0=I0';
I45=I45';
I90=I90';
I135=I135';

radius_0=radius_0';
radius_45=radius_45';
radius_90=radius_90';
radius_135=radius_135';

%% Removing nan
if or(sum(isnan(rho_all))>0,sum(isnan(delta_all))>0)
    selnonan=(~(isnan(rho_all))==1)&(~(isnan(delta_all))==1);
    X=X(selnonan);
    Y=Y(selnonan);
    T=T(selnonan);
    Intensity=Intensity(selnonan);
    Z =Z(selnonan);
    Dummy_sigma_X=Dummy_sigma_X(selnonan);
    Dummy_sigma_Y=Dummy_sigma_Y(selnonan);
    rho_all=rho_all(selnonan);
    delta_all=delta_all(selnonan);
    
    sigma_0=sigma_0(selnonan);
    sigma_45=sigma_45(selnonan);
    sigma_90=sigma_90(selnonan);
    sigma_135=sigma_135(selnonan);
    
    sigmabg_0=sigmabg_0(selnonan);
    sigmabg_45=sigmabg_45(selnonan);
    sigmabg_90=sigmabg_90(selnonan);
    sigmabg_135=sigmabg_135(selnonan);
     sigmabg=sigmabg(selnonan);

    P0vect_all=P0vect_all(selnonan);
    P45vect_all=P45vect_all(selnonan);

    I0=I0(selnonan);
    I45=I45(selnonan);
    I90=I90(selnonan);
    I135=I135(selnonan);

    radius_0=radius_0(selnonan);
    radius_45=radius_45(selnonan);
    radius_90=radius_90(selnonan);
    radius_135=radius_135(selnonan);

end

%%
% tab_traj_4x4ALL=createfullmatrix(data.tab_traj_4x4,data.param_t_j,data.param_t_i,data.tab_trajcoupled,data.tab_traj_allcoupled);
tab_traj_4x4ALL=createfullmatrix(data.tab_traj_4x4,data.param_t_j,data.param_t_i,data.tab_trajcoupled,data.tab_trajcoupled);

Xa=tab_traj_4x4ALL(data.param_t_j, :)';
Ya=tab_traj_4x4ALL(data.param_t_i, :)';
Ta = tab_traj_4x4ALL(data.param_t_tps, :)';

I0a =((2*sqrt(pi)).*tab_traj_4x4ALL(data.param_t_r0,:).*tab_traj_4x4ALL(data.param_t_alpha,:))';
I45a=I0a;
I90a=I0a;
I135a=I0a;
 
sigma_0a=(sqrt(tab_traj_4x4ALL(data.param_t_sig2,:)))';
sigma_45a=sigma_0a;
sigma_90a=sigma_0a;
sigma_135a=sigma_0a;

sigmabga=(sqrt(tab_traj_4x4ALL(data.param_t_sig2_b,:)))';
sigmabg_0a=sigmabga;
sigmabg_45a=sigmabg_0a;
sigmabg_90a=sigmabg_0a;
sigmabg_135a=sigmabg_0a;
 
radius_0a=(sqrt(tab_traj_4x4ALL(data.param_t_r0,:)))';
radius_45a=radius_0a;
radius_90a=radius_0a;
radius_135a=radius_0a;  

Intensitya=4*I0a;

Za = 10.*(Intensitya/max(Intensitya(:)));

Dummy_sigma_Xa=1.*ones(size(Xa));
Dummy_sigma_Ya=1.*ones(size(Xa));
rho_alla=1000.*ones(size(Xa));
delta_alla=1000.*ones(size(Xa));
P0vect_alla=1000.*ones(size(Xa));
P45vect_alla=1000.*ones(size(Xa));




%%
subset4x4=[ones(size(X));zeros(size(Xa))];
X=[X;Xa];
Y=[Y;Ya];
T=[T;Ta];
I0=[I0;I0a];
I45=[I45;I45a];
I90=[I90;I90a];
I135=[I135;I135a];
sigma_0=[sigma_0;sigma_0a];
sigma_45=[sigma_45;sigma_45a];
sigma_90=[sigma_90;sigma_90a];
sigma_135=[sigma_135;sigma_135a];

sigmabg=[sigmabg;sigmabga];
sigmabg_0=[sigmabg_0;sigmabg_0a];
sigmabg_45=[sigmabg_45;sigmabg_45a];
sigmabg_90=[sigmabg_90;sigmabg_90a];
sigmabg_135=[sigmabg_135;sigmabg_135a];

radius_0=[radius_0;radius_0a];
radius_45=[radius_45;radius_45a];
radius_90=[radius_90;radius_90a];
radius_135=[radius_135;radius_135a];
Intensity=[Intensity;Intensitya];
Z=[Z;Za];
Dummy_sigma_X=[Dummy_sigma_X;Dummy_sigma_Xa];
Dummy_sigma_Y=[Dummy_sigma_Y;Dummy_sigma_Ya];
rho_all=[rho_all;rho_alla];
delta_all=[delta_all;delta_alla];

P0vect_all=[P0vect_all;P0vect_alla];
P45vect_all=[P45vect_all;P45vect_alla];

clearvars data
PALMsiever('X','Y')

