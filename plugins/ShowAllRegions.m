function ShowAllRegions(handles)

subset = getSubset(handles);
subset4x4=evalin('base','subset4x4');
subset=subset&subset4x4;

X=getX(handles); X=X(subset);
Y=getY(handles); Y=Y(subset);
delta_all=evalin('base','delta_all');
rho_all=evalin('base','rho_all');
scalesticks=str2double(get(handles.edit18,'String'));
numAreas=evalin('base','numAreas');

minCdelta=str2double(get(handles.edit15,'String'));
maxCdelta=str2double(get(handles.edit14,'String'));
minCrho=str2double(get(handles.edit17,'String'));
maxCrho=str2double(get(handles.edit16,'String'));


h3=figure('units','normalized','Position',[0 0 0.5 0.5]);
set(gcf,'Color','w')
nh1 = copyobj(handles.axes1,h3);
set(nh1,'OuterPosition',[0 0 1 1]);
axis(nh1,'equal');
set(nh1,'color','none');
set(gca, 'XDir', 'reverse');

cm = colormap(handles.axes1);
colormap(nh1,cm);
hold on
u=((1.*cosd(rho_all)));
v=(1.*sind(rho_all));
u=u(subset);
v=v(subset);

quiverc3modif(X,Y,u,v,delta_all(subset),minCdelta,maxCdelta,'jet','scale',scalesticks);
title(['Sticks color = > \delta :[' num2str(minCdelta) ',' num2str(maxCdelta) ']' ]);
hold on

for i=1:numAreas
    vertices=evalin('base',['vertices_' num2str(i)]);
    plot([vertices(:,1);vertices(1,1)],[vertices(:,2);vertices(1,2)],'w');
    text(vertices(1,1),vertices(1,2),num2str(i),'Color','w','FontSize',14);

end
[x0,y0]=insert_scalebar(handles,gca);

