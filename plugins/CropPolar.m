%%
function CropPolar(handles)

subset = getSubset(handles);
subset4x4=evalin('base','subset4x4');
subset=subset&subset4x4;

X=getX(handles); X=X(subset);
Y=getY(handles); Y=Y(subset);
delta_all=evalin('base','delta_all');
rho_all=evalin('base','rho_all');
scalesticks=str2double(get(handles.edit18,'String'));
numAreas=evalin('base','numAreas');

minCdelta=str2double(get(handles.edit15,'String'));
maxCdelta=str2double(get(handles.edit14,'String'));
minCrho=str2double(get(handles.edit17,'String'));
maxCrho=str2double(get(handles.edit16,'String'));

h=figure('units','normalized','Position',[0 0 0.5 0.5]);
% handles.axes1 = gca;
% redraw(handles)
nh = copyobj(handles.axes1,h);
set(nh,'OuterPosition',[0 0 1 1]);
axis(nh,'equal');
set(nh,'color','none');
set(gca, 'XDir', 'reverse');

cm = colormap(handles.axes1);
colormap(nh,cm);

vertices1=0;
h2 = impoly; %define
vertices=wait(h2); %waiting double clic
% delete(h2)

[inpoly onboundary] = inpolygon(getX(handles),getY(handles),vertices(:,1),vertices(:,2));

selPoints=inpoly;

selRho=rho_all(selPoints&subset);
selDelta=delta_all(selPoints&subset);


close(h)
%%
h3=figure('units','normalized','Position',[0 0 0.5 0.5]);

set(gcf,'Color','w')
% subplot(3,1,1)
nh1 = copyobj(handles.axes1,h3);
set(nh1,'OuterPosition',[0 0 1 1]);
axis(nh1,'equal');
set(nh1,'color','none');
set(gca, 'XDir', 'reverse');

cm = colormap(handles.axes1);
colormap(nh1,cm);
hold on
plot([vertices(:,1);vertices(1,1)],[vertices(:,2);vertices(1,2)],'w')

u=((1.*cosd(rho_all)));
v=(1.*sind(rho_all));
u=u(subset);
v=v(subset);
hold on
quiverc3modif(X,Y,u,v,delta_all(subset),minCdelta,maxCdelta,'jet','scale',scalesticks);
title(['Sticks color = > \delta :[' num2str(minCdelta) ',' num2str(maxCdelta) ']' ])


% return

figure('units','normalized','Position',[0 0 0.4 0.8]);
set(gcf,'Color','w')
subplot(2,1,1)
hist(selRho,0:10:180)
xlim([-10 190])
ylabel('Frequency')
xlabel('Deg')
title(['\rho - Mean: ' num2str(mean(selRho),'%0.1f') '�, Std: ' num2str(std(selRho),'%0.1f') '�'])

subplot(2,1,2)
hist(selDelta,0:10:180)
xlim([-10 190])
xlim([-10 190])
ylabel('Frequency')
xlabel('Deg')
title(['\delta - Mean: ' num2str(mean(selDelta),'%0.1f') '�, Std: ' num2str(std(selDelta),'%0.1f') '�'])

%%

button = questdlg('Do you want to save this region?','Save region');

if strcmp(button,'Yes')
numAreas=numAreas+1;
assignin('base',['vertices_' num2str(numAreas)],vertices);
assignin('base',['selPoints_' num2str(numAreas)],selPoints);
assignin('base','numAreas',numAreas);
end
