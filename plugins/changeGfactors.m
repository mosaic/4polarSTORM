%% Recalculate delta and rho values

function changeGfactors(handles)

G1=evalin('base','factorg1');
G2=evalin('base','factorg2');
G45=evalin('base','factorg45');
G90=evalin('base','factorg90');
G0_45=evalin('base','factor0_45');
subset4x4=evalin('base','subset4x4');

choice = questdlg('Are you sure? Be aware that this change is irreversible','Warning!!!!');
if strcmp(choice,'Yes')
prompt = {'G1: ','G2: ', 'G45: ', 'G90: ','G0_45'};
dlg_title = 'Input';
num_lines = 1;
defaultans = {num2str(G1),num2str(G2),num2str(G45),num2str(G90),num2str(G0_45)};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
if ~isempty(answer)
I0=evalin('base','I0');
I45=evalin('base','I45');
I90=evalin('base','I90');
I135=evalin('base','I135');

G1=str2double(answer{1});
G2=str2double(answer{2});
G45=str2double(answer{3});
G90=str2double(answer{4});
G0_45=str2double(answer{5});
[delta,rho1,P0vect,P45vect]=Fcn_dg_4x4_modif(I0,I45,I90,I135,G1,G2,G45,G90,G0_45);
delta(~subset4x4)=1000;
rho1(~subset4x4)=1000;
P0vect(~subset4x4)=1000;
P45vect(~subset4x4)=1000;

if or(sum(isnan(rho1))>0,sum(isnan(delta))>0)    
    assignin('base','delta_all',delta.*(180/pi))
    assignin('base','rho_all',rho1.*(180/pi))
    assignin('base','P0vect_all',P0vect)
    assignin('base','P45vect_all',P45vect)
    selnonan=(~(isnan(rho1))==1)&(~(isnan(delta))==1);
    
     
    w = evalin('base','whos');
    for a = 1:length(w) 
        tempval = evalin('base',w(a).name); 
        if size(tempval,1)==size(I0,1)
            assignin('base',w(a).name,tempval(selnonan,:))
        end
    end
       

else
    assignin('base','delta_all',delta.*(180/pi))
    assignin('base','rho_all',rho1.*(180/pi))
    assignin('base','P0vect_all',P0vect)
    assignin('base','P45vect_all',P45vect)
    
end

end

G1=str2double(answer{1});
G2=str2double(answer{2});
G45=str2double(answer{3});
G90=str2double(answer{4});
G0_45=str2double(answer{5});
    assignin('base','factorg1',G1)
    assignin('base','factorg2',G2)
    assignin('base','factorg45',G45)
    assignin('base','factorg90',G90)
    assignin('base','factor0_45',G0_45)
% updateTable(handles);
end
handles.settings.N=evalin('base','size(X,1)');
% handles.settings.N
% pause


% function updateTable(handles)
% XPosition=evalin('base',handles.settings.varx);
% data = get(handles.tParameters,'Data');
% subset = true(length(XPosition),1);
% rows = get(handles.tParameters,'RowName');
% for irow=1:length(rows)
%     subset = subset & evalin('base',[rows{irow} '>=' num2str(data{irow,1}) '&' ...
%         rows{irow} '<=' num2str(data{irow,2})]);
% end
% assignin('base','subset',subset)
% 
% cols = get(handles.tParameters,'ColumnName');
% rows = get(handles.tParameters,'RowName');
% for irow=1:length(rows)
%     var = evalin('base',rows{irow});
%     for icol=3:length(cols)
%         fev = evalin('base',[cols{icol} '(' rows{irow} '(subset))']);
%         if strcmp(cols{icol},'min')
%             if fev>=0
%                 fev = fev * .999;
%             else
%                 fev = fev / .999;
%             end
%         elseif strcmp(cols{icol},'max')
%             if fev >=0
%                 fev = fev / .999;
%             else
%                 fev = fev * .999;
%             end
%         end
%         data{irow,icol} = fev;
%     end
% end
% set(handles.tParameters,'Data',data)
