function ShowRegion(handles)

subset = getSubset(handles);
subset4x4=evalin('base','subset4x4');
subset=subset&subset4x4;

X=getX(handles); X=X(subset);
Y=getY(handles); Y=Y(subset);
delta_all=evalin('base','delta_all');
rho_all=evalin('base','rho_all');
scalesticks=str2double(get(handles.edit18,'String'));
numAreas=evalin('base','numAreas');

minCdelta=str2double(get(handles.edit15,'String'));
maxCdelta=str2double(get(handles.edit14,'String'));
minCrho=str2double(get(handles.edit17,'String'));
maxCrho=str2double(get(handles.edit16,'String'));

x = inputdlg('Enter region number:',...
             'Region Selection', 1);
i = str2num(x{:}); 
vertices=evalin('base',['vertices_' num2str(i)]);
selPoints=evalin('base',['selPoints_' num2str(i)])&subset;


h3=figure('units','normalized','Position',[0 0 0.5 0.5]);
set(gcf,'Color','w')
nh1 = copyobj(handles.axes1,h3);
set(nh1,'OuterPosition',[0 0 1 1]);
axis(nh1,'equal');
set(nh1,'color','none');
set(gca, 'XDir', 'reverse');

cm = colormap(handles.axes1);
colormap(nh1,cm);
hold on
u=((1.*cosd(rho_all)));
v=(1.*sind(rho_all));
u=u(subset);
v=v(subset);

quiverc3modif(X,Y,u,v,delta_all(subset),minCdelta,maxCdelta,'jet','scale',scalesticks);
hold on
plot([vertices(:,1);vertices(1,1)],[vertices(:,2);vertices(1,2)],'w');
title(['ColorDelta: [' num2str(minCdelta) ',' num2str(maxCdelta) ']' ]);

[x0,y0]=insert_scalebar(handles,gca);

selRho=rho_all(selPoints);
selDelta=delta_all(selPoints);


h4=figure('units','normalized','Position',[0 0 0.4 0.8]);
set(gcf,'Color','w')
subplot(3,1,1)
hist(selRho,0:10:180)
xlim([-10 190])
ylabel('Frequency')
xlabel('Deg')
title(['Region: ' num2str(i) ', \rho - Mean: ' num2str(mean(selRho),'%0.1f') '�, Std: ' num2str(std(selRho),'%0.1f') '�'])


subplot(3,1,2)
hist(selDelta,0:10:180)
xlim([-10 190]);
ylabel('Frequency')
xlabel('Deg')
title(['Region: ' num2str(i) ', \delta - Mean: ' num2str(mean(selDelta),'%0.1f') '�, Std: ' num2str(std(selDelta),'%0.1f') '�'])

Rho=selRho;
ComplexRho = exp(1i*Rho*pi/180);
AvgComplexRho = mean(ComplexRho);       %%% It will be centered around the mean, and not around the median!!!
AvgRho = atan2(imag(AvgComplexRho),real(AvgComplexRho))*180/pi;
Centered_Rho = Rho-AvgRho ;            
Centered_Rho=(Centered_Rho<-90).*(Centered_Rho+180)+(Centered_Rho>90).*(Centered_Rho-180)+((Centered_Rho>-90) & (Centered_Rho<90)).*Centered_Rho;  

subplot(3,1,3)
hist(Centered_Rho,-90:10:90)
xlim([-90 90])
ylabel('Frequency')
xlabel('Deg')
title(['Centered Rho. Region: ' num2str(i)  ', Std: ' num2str(std(Centered_Rho),'%0.1f') '�'])


%%

h5=figure('units','normalized','Position',[0 0 0.5 0.5]);
XR=getX(handles); X=XR(selPoints);
YR=getY(handles); Y=YR(selPoints);
set(gcf,'Color','w')
nh2 = copyobj(handles.axes1,h5);
set(nh2,'OuterPosition',[0 0 1 1]);
axis(nh2,'equal');
set(nh2,'color','none');
set(gca, 'XDir', 'reverse');
xlim([min(X(:)) max(X(:))]);
ylim([min(Y(:)) max(Y(:))]);

cm = colormap(handles.axes1);
colormap(nh2,cm);
hold on
u=((1.*cosd(rho_all)));
v=(1.*sind(rho_all));
u=u(selPoints);
v=v(selPoints);
quiverc3modif(X,Y,u,v,delta_all(selPoints),minCdelta,maxCdelta,'jet','scale',scalesticks);
title(['Sticks color = > \delta :[' num2str(minCdelta) ',' num2str(maxCdelta) ']' ])
[x0,y0]=insert_scalebar(handles,gca);

%%
h6=figure('units','normalized','Position',[0 0 0.5 0.5]);

XR=getX(handles); X=XR(selPoints);
YR=getY(handles); Y=YR(selPoints);

set(gcf,'Color','w')
nh2 = copyobj(handles.axes1,h6);
set(nh2,'OuterPosition',[0 0 1 1]);
axis(nh2,'equal');
set(nh2,'color','none');
set(gca, 'XDir', 'reverse');
xlim([min(X(:)) max(X(:))])
ylim([min(Y(:)) max(Y(:))])

cm = colormap(handles.axes1);
colormap(nh2,cm);
hold on
u=((1.*cosd(rho_all)));
v=(1.*sind(rho_all));
u=u(selPoints);
v=v(selPoints);

quiverc3modif(X,Y,u,v,rho_all(selPoints),minCrho,maxCrho,'hsv','scale',scalesticks);
title(['Sticks color = > \rho :[' num2str(minCrho) ',' num2str(maxCrho) ']' ]);
insert_scalebar(handles,gca,x0,y0);