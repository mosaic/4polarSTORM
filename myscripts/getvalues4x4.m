function [delta_all,rho_all,posx,posy,Itotal,sigma_0,sigma_45,sigma_90,sigma_135,P0vect_all,P45vect_all,I0,I45,I90,I135,frame,radius_0,radius_45,radius_90,radius_135,sigmabg_0,sigmabg_90,sigmabg_45,sigmabg_135,sigmabg] = getvalues4x4(tab_traj,factorg1,factorg2, factorg45, factorg90, factor0_45)
%%%% 
STHERM_param ;

%% Spots right and left sides.

   tab_traj_dg = tab_traj ;
   tab_traj   = tab_traj_dg(:, 1:4:end) ; %% Right side
   tab_traj_45 = tab_traj_dg(:, 2:4:end) ; %% Left side
   tab_traj_90   = tab_traj_dg(:, 3:4:end) ; %% Right side
   tab_traj_135 = tab_traj_dg(:, 4:4:end) ; %% Left side

%% Spots in the zone
pos_traj_in_zone = 1:size(tab_traj,2);


  I0 =(2*sqrt(pi)).*tab_traj(param_t_r0, pos_traj_in_zone).*tab_traj(param_t_alpha, pos_traj_in_zone) ;
  I45 =(2*sqrt(pi)).*tab_traj_45(param_t_r0, pos_traj_in_zone).* tab_traj_45(param_t_alpha, pos_traj_in_zone) ;
  I90 =(2*sqrt(pi)).*tab_traj_90(param_t_r0, pos_traj_in_zone).*tab_traj_90(param_t_alpha, pos_traj_in_zone) ;
  I135 =(2*sqrt(pi)).*tab_traj_135(param_t_r0, pos_traj_in_zone).* tab_traj_135(param_t_alpha, pos_traj_in_zone) ;
  posx=tab_traj(param_t_j, pos_traj_in_zone);
  posy=tab_traj(param_t_i, pos_traj_in_zone);
  
  sigma_0=sqrt(tab_traj(param_t_sig2, pos_traj_in_zone));
  sigma_45=sqrt(tab_traj_45(param_t_sig2, pos_traj_in_zone));
  sigma_90=sqrt(tab_traj_90(param_t_sig2, pos_traj_in_zone));
  sigma_135=sqrt(tab_traj_135(param_t_sig2, pos_traj_in_zone));
  
  radius_0=(tab_traj(param_t_r0, pos_traj_in_zone));
  radius_45=(tab_traj_45(param_t_r0, pos_traj_in_zone));
  radius_90=(tab_traj_90(param_t_r0, pos_traj_in_zone));
  radius_135=(tab_traj_135(param_t_r0, pos_traj_in_zone));
  
  sigmabg_0=sqrt(tab_traj(param_t_sig2_b, pos_traj_in_zone));
  sigmabg_45=sqrt(tab_traj_45(param_t_sig2_b, pos_traj_in_zone));
  sigmabg_90=sqrt(tab_traj_90(param_t_sig2_b, pos_traj_in_zone));
  sigmabg_135=sqrt(tab_traj_135(param_t_sig2_b, pos_traj_in_zone));
  
  sigmabg=0.25*(sigmabg_0+sigmabg_45+sigmabg_90+sigmabg_135);
  
  frame=max([tab_traj(param_t_tps, pos_traj_in_zone)' tab_traj_45(param_t_tps, pos_traj_in_zone)' tab_traj_90(param_t_tps, pos_traj_in_zone)' tab_traj_135(param_t_tps, pos_traj_in_zone)'],[],2)';

  G1=factorg1;
  G2=factorg2;
  G45=factorg45;
  G90=factorg90;
  G0_45=factor0_45;

  [delta_all,rho_all,P0vect_all,P45vect_all]=Fcn_dg_4x4_modif(I0,I45,I90,I135,G1,G2,G45,G90,G0_45);
    
   
  Itotal=I0+I45+I90+I135;
   

end
