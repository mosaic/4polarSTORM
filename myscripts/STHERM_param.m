%% STHERM : 
%%
%% (*) STochastic Optical Reconstruction Microscopy 
%%
%% ALGORITHM AUTHOR:
%% N. BERTAUX
%% C. VALADES-CRUZ
%% Institut Fresnel, Equipe PhyTI
%% Date = mars 2012

%% DO NOT MODIFY NEXT LINES
%% DO NOT MODIFY NEXT LINES
%% DO NOT MODIFY NEXT LINES
%% DO NOT MODIFY NEXT LINES
%% DO NOT MODIFY NEXT LINES


global wn r0 pfa 
global DEBUG DISPLAY 
global char_slash


%% USER PARAMETERS : CAN BE MODIFIED
%% USER PARAMETERS : CAN BE MODIFIED
%% USER PARAMETERS : CAN BE MODIFIED
%% USER PARAMETERS : CAN BE MODIFIED
%% USER PARAMETERS : CAN BE MODIFIED


%% Select based on the system
%% windows 
%char_slash = '\' ;
%% unix
char_slash = '/' ;

%%% Window Test
wn = 15 ; %% odd

%%% Gaussian raudius
r0 = 1.6 ;

%%% Probability of false alarm
%%% 28 =>1E-7	24 =>1E-6
pfa = 24 ;


%% mode debug
DEBUG = 0 ; %% [0/1]
DISPLAY = 1 ; %% [0/1] 




%% DO NOT MODIFY NEXT LINES
%% DO NOT MODIFY NEXT LINES
%% DO NOT MODIFY NEXT LINES
%% DO NOT MODIFY NEXT LINES
%% DO NOT MODIFY NEXT LINES

global param_p_num param_p_i param_p_j param_p_alpha param_p_sig2 param_p_r0 param_p_ok
global param_t_num param_t_i param_t_j param_t_r0 param_t_sig2  param_t_tps
global nb_param_t 

%%% index of particles
%%% liste_part, compat detect_reconnex
param_p_num         = 1 ;
param_p_i           = 2 ;
param_p_j           = 3 ;
param_p_alpha       = 4 ; 
param_p_sig2        = 5 ; %% noise variance
param_p_r0          = 6 ;
param_p_ok          = 7 ;

%%% liste_traj
nb_param_t = 8 ;
param_t_num         = 1 ;
param_t_i           = 2 ;  %% spot position
param_t_j           = 3 ;  %% spot position
param_t_r0          = 4 ;  %% PSF radius
param_t_sig2        = 5 ;  %% variance of position
param_t_alpha       = 6 ;  %% intensity
param_t_sig2_b      = 7 ;  %% noise variance
param_t_tps         = 8 ;  %% time

