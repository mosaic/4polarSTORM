
function [delta,rho1,P0vect,P45vect]=Fcn_dg_4x4_modif(I0,I45,I90,I135,G1,G2,G45,G90,G0_45)

P0vect=  (I0-G90.*I90)./(I0+G90.*I90);
P45vect= ((1-G2+G1).*I45-(1-G1+G2).*G45.*I135)./((1-G2-G1).*(I45+G45.*I135));

P0vect=P0vect.*G0_45;
xdata=0:0.01:pi;
ydata=(sinc2(0:0.01:pi)).^2;


sum2P0P45=P0vect.^2+P45vect.^2;
delta = interp1(ydata,xdata,sum2P0P45);
rho= atan2(P45vect,P0vect);
rho1=rho;
rho1(rho<0)=rho1(rho<0)+2*pi;
rho1=0.5.*rho1;