function saveAllRegions()

numAreas=evalin('base','numAreas');

w = evalin('base','whos');
for a = 1:length(w) 
    s.(w(a).name) = evalin('base',w(a).name); 
end
clear a
clear w
% filename=s.filename;
originalname=s.originalname;
factor0_45=s.factor0_45;
factorg1=s.factorg1;
factorg2=s.factorg2;
factorg45=s.factorg45;
factorg90=s.factorg90;

[filename, pathname, filterindex] = uiputfile([originalname(1:end-4) '_Region.mat'],'Save Region as');
% return

for i=1:numAreas
    selPoints=evalin('base',['selPoints_' num2str(i)]);
    vertices=evalin('base',['vertices_' num2str(i)]);
    
    subset4x4=s.subset4x4(selPoints);
    
    X=s.X(selPoints);
    Y=s.Y(selPoints);
    T=s.T(selPoints);
    Intensity=s.Intensity(selPoints);
    Z=s.Z(selPoints);

    Dummy_sigma_X=s.Dummy_sigma_X(selPoints);
    Dummy_sigma_Y=s.Dummy_sigma_Y(selPoints);
    
    rho_all=s.rho_all(selPoints);
    delta_all=s.delta_all(selPoints);
    
    sigma_0=s.sigma_0(selPoints);
    sigma_45=s.sigma_45(selPoints);
    sigma_90=s.sigma_90(selPoints);
    sigma_135=s.sigma_135(selPoints);
    
    sigmabg=s.sigmabg(selPoints);
    sigmabg_0=s.sigmabg_0(selPoints);
    sigmabg_45=s.sigmabg_45(selPoints);
    sigmabg_90=s.sigmabg_90(selPoints);
    sigmabg_135=s.sigmabg_135(selPoints);

    P0vect_all=s.P0vect_all(selPoints);
    P45vect_all=s.P45vect_all(selPoints);

    I0=s.I0(selPoints);
    I45=s.I45(selPoints);
    I90=s.I90(selPoints);
    I135=s.I135(selPoints);

    radius_0=s.radius_0(selPoints);
    radius_45=s.radius_45(selPoints);
    radius_90=s.radius_90(selPoints);
    radius_135=s.radius_135(selPoints);
     save([pathname filename(1:end-4) '_' num2str(i) '.mat'], '-regexp', '^(?!(s|numAreas)$).') 
end
f = msgbox('Saving Completed');