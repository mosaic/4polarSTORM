function [x0,y0]=insert_scalebar(handles,axesfigure,varargin)
 
[minX, maxX, minY, maxY] = getBounds(handles);

res = getRes(handles);

% Calculate pixel sizes
pxx = (maxX-minX)/res;
pxy = (maxY-minY)/res;
w=str2double(get(handles.edit20,'String'))*1000/str2double(get(handles.edit19,'String'));
% get(handles.checkbox4,'Value');

if get(handles.checkbox4,'Value')
if pxx==pxy
    if nargin==2
        axes(axesfigure)
        [x0,y0] = ginput(1);
        line([x0 x0-w],[y0 y0],'Color','w','LineWidth',5);
    else
        axes(axesfigure)
        x0=varargin{1};
        y0=varargin{2};
        line([x0 x0-w],[y0 y0],'Color','w','LineWidth',5);
    end
else
    errordlg('Please be sure both pixel dimension are equal (Botton 1:1).', 'Error');
     x0=[];
    y0=[];
end
else
    x0=[];
    y0=[];
end