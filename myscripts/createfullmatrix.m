
function tab_traj_4x4ALL=createfullmatrix(tab_traj_4x4,param_t_j,param_t_i,tab_trajcoupled,tab_traj_allcoupled)
% close all

% j1=floor(min(tab_trajcoupled(param_t_j, 2:2:end)));
% j2=ceil(max(tab_trajcoupled(param_t_j, 2:2:end)));
% i1=floor(min(tab_trajcoupled(param_t_i, 2:2:end)));
% i2=ceil(max(tab_trajcoupled(param_t_i, 2:2:end)));

j1=floor(min(tab_traj_4x4(param_t_j, 2:4:end)));
j2=ceil(max(tab_traj_4x4(param_t_j, 2:4:end)));
i1=floor(min(tab_traj_4x4(param_t_i, 2:4:end)));
i2=ceil(max(tab_traj_4x4(param_t_i, 2:4:end)));

j1a=floor(min(tab_traj_4x4(param_t_j, 1:4:end)));
j2a=ceil(max(tab_traj_4x4(param_t_j, 1:4:end)));
i1a=floor(min(tab_traj_4x4(param_t_i, 1:4:end)));
i2a=ceil(max(tab_traj_4x4(param_t_i, 1:4:end)));

meandispj=mean(tab_trajcoupled(param_t_j,1:2:end)-tab_trajcoupled(param_t_j,2:2:end));
meandispi=mean(tab_trajcoupled(param_t_i,1:2:end)-tab_trajcoupled(param_t_i,2:2:end));
CoupledXY=tab_trajcoupled(2:3,:);
NocoupledXY=tab_traj_allcoupled(2:3,:);

selcouples=ismember(NocoupledXY',CoupledXY','rows')';
in=inpolygon(NocoupledXY(1,:),NocoupledXY(2,:),[i1 i1 i2 i2 i1],[j1 j2 j2 j1 j1]);
in2=inpolygon(NocoupledXY(1,:),NocoupledXY(2,:),[i1a i1a i2a i2a i1a],[j1a j2a j2a j1a j1a]);

New_4x4_nocoupled=tab_traj_allcoupled(:,(in)&(~selcouples));
New_4x4_nocoupled(param_t_j,:)=New_4x4_nocoupled(param_t_j,:)-meandispj;
New_4x4_nocoupled(param_t_i,:)=New_4x4_nocoupled(param_t_i,:)-meandispi;
New_4x4_nocoupled=[New_4x4_nocoupled;nan(1,size(New_4x4_nocoupled,2))];

New_4x4_nocoupleddown=tab_traj_allcoupled(:,(in2)&(~selcouples));
New_4x4_nocoupleddown=[New_4x4_nocoupleddown;nan(1,size(New_4x4_nocoupleddown,2))];

Matrixtemp= [New_4x4_nocoupled New_4x4_nocoupleddown];
% tab_traj_4x4ALL=[tab_traj_4x4(:,1:4:end) Matrixtemp];
tab_traj_4x4ALL=[Matrixtemp];
tab_traj_4x4ALL=tab_traj_4x4ALL(:,sort(tab_traj_4x4ALL(8,:)));

